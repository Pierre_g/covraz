# Objectives and methods
CovRaz is a tool for the detection of CNV on sequencing data by amplification on a gene panel. 
The principle of CovRaz is to compare the depth profile of each gene in order to detect those which are expected as outlier, which can then indicate the presence of a CNV within this gene. method in order to more calmly check the possible CNVs present.

__3 methods__
- Patent vs Population: Use of a reference population, limited in outlier, to visualize the way in which a patient is positioned in relation to this population.
- Z-score: Calculation of the z-score in order to statistically determine if the difference in depth of a gene for a given patient compared to a data set is associated with the presence of CNV
- CovCopCan :  Tool developed by the team of Derouault _et al_ (1) is a tool that allows the rapid and easy detection of CNVs in inherited diseases, as well as somatic data of patients with cancer, even with a low ratio of cancer cells to healthy cells. (https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1007503#references)

# Getting started

## Installation
To perform the installation you can git clone this project and then get the executable too_CovRaZ.py

## Dépendancies
All the dependencies are present in the yml file which can create a CONDA environment which can be used directly.
__WARNING__

The VCF.py file must be in the same folder as too_CovRaZ to allow the import of the VCF module.

## Inputs files
The input equivalents are indicated in _italics_, in order to visualize the necessary formats.

__Requis__
- Cover matrix: _ampliconscoverage.csv_
- Cover matrix in CovCopCan format: _ampliconsCovCopCan.tsv_
- Experimental design: _designExp.tsv_
- Reference dataset to visualise the depths in relation to a population:
    - refWTO.tsv
- CovCopCan jar file (Disponible: https://git.unilim.fr/merilp02/CovCopCan/tree/master): _CovCopCan-1.3.3.jar_
 
__Optionnal__
- Reference matrix (s) for z-score and / or CovCopCan:
    - _refAll.tsv_
    - _refWTO.tsv_
    - _refRAND.tsv_
- File detailing the genes on which to focus the analysis: _selectGene.tsv_

## Example shell command

python tool_CovRaZ.py -m ./Input_files/ampliconscoverage.csv -c ./Input_files/ampliconsCovCopCan.tsv  -d ./Input_files/designExp.tsv  -r ./Input_files/refWTO.tsv -rZ ./Input_files/refAll.tsv ./Input_files/refWTO.tsv -nZ ALL WTO -rC ./Input_files/refRAND.tsv ./Input_files/refWTO.tsv -nC RAND WTO -e ../CovCopCan-1.3.3.jar -o ./Output_files/ -b ./Input_files/cytoband.txt  -g ./Input_files/selectGene.tsv -u Test_CovRaZ -jp /usr/bin


