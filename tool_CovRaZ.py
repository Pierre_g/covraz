import itertools
import pandas as pd
import VCF
import numpy as np
import glob
import os
import argparse
import plotly.express as px
import base64


#################################################################
# STRUCTURE#
#################################################################
#####RATIO######
####Fonction####
# -ratioGeneSumMedTrans(tabo, by, log=True)
# -ratioGeneSumBase(tabo, by, log=True)
# -concatOrderCov(
# -plotGraph(matrix,runName)

####Z-SCORES####
####Fonction####
# -mad(a, med, axis)
# -zscoreCalul(tabo)

####CovCopCan####

####Fonction####
##VarPatient##
# concatFrameCov(listeFilename)
# extract_end(string)
# identifyGene(framevcf, designframe, by="gene")
# orderCovCop(tabidSeg)
##Seuil##
# concatCovRef(tabrun, tabref)
# recupPathCov(path)
# concatCov(tab, genlist)
###Compléments###


#########################
# RATIOS#
#########################
def ratioGeneSumMedTrans(tabo, by, log=True):
    """Fonction utilisée pour faire la visualisation en BOXPLOT
    Input:
    -Matrice de reouvrement en 2D
    -Echelle: 'gene','exon','amplicon'
    -Transformation par le log: True/False
    Output:
    -Matrice 2D contenant les ratios centré réduit de l'échelle souhaité"""
    segName = tabo.index
    dicoSeg = {}

    frameout = pd.DataFrame(columns=tabo.columns)

    tab = (tabo / tabo.sum())

    for pattern in segName:

        if by == "gene":
            gene = pattern.split("Ex")[0]
            if gene not in dicoSeg.keys():
                dicoSeg[gene] = []
            dicoSeg[gene].append(pattern)

        if by == "exon":

            nameEx = pattern.split("Ex")[0]
            patternEx = pattern.split("Ex")[1]
            while not patternEx.isnumeric():
                patternEx = patternEx[:-1]

            exonName = nameEx + 'Ex' + patternEx

            if exonName not in dicoSeg.keys():
                dicoSeg[exonName] = []
            dicoSeg[exonName].append(pattern)

        if by == "amplicon":

            if pattern not in dicoSeg.keys():
                dicoSeg[pattern] = []
            dicoSeg[pattern].append(pattern)

    for name in dicoSeg.keys():
        tabGene = tab.loc[dicoSeg[name], :]
        outSum = pd.DataFrame(tabGene.sum(axis=0)).T

        if log == False:
            med = outSum.median(axis=1)[0]

            outSum = outSum - med
        outSum["Name"] = name

        frameout = pd.concat([frameout, outSum], ignore_index=False)
    frameout.set_index("Name", inplace=True)
    if log:
        frameout2 = frameout.copy()
        for cols in frameout2.columns:
            frameout2[cols] = frameout2[cols].astype('float64')
            frameout2[cols] = np.log2((frameout2.loc[frameout2[cols].notna(), cols]), dtype='float64')
        return frameout2
    else:
        return frameout


def ratioGeneSumBase(tabo, by, log=True):
    """Fonction utilisée pour faire la visualisation en BOXPLOT
    Input:
    -Matrice de reouvrement en 2D
    -Echelle: 'gene','exon','amplicon'
    -Transformation par le log: True/False
    Output:
    -Matrice 2D contenant les ratios réduit de l'échelle souhaité avec une transofrmation par le log  si elle est
    utilisée pour le calcul du z-score"""

    segName = tabo.index
    dicoSeg = {}

    frameout = pd.DataFrame(columns=tabo.columns)
    # Normalisation par les effectifs totaux des amplicons de chaque patient

    tab = (tabo / tabo.sum())

    # tab["Name"] = tabo.iloc[:, 0].values

    for pattern in segName:

        if by == "gene":
            gene = pattern.split("Ex")[0]
            if gene not in dicoSeg.keys():
                dicoSeg[gene] = []
            dicoSeg[gene].append(pattern)
            # Récupère tous les gènes du tableau

        if by == "exon":

            nameEx = pattern.split("Ex")[0]
            patternEx = pattern.split("Ex")[1]
            while patternEx.isnumeric() == False:
                patternEx = patternEx[:-1]

            exonName = nameEx + 'Ex' + patternEx

            if exonName not in dicoSeg.keys():
                dicoSeg[exonName] = []
            dicoSeg[exonName].append(pattern)

        if by == "amplicon":

            if pattern not in dicoSeg.keys():
                dicoSeg[pattern] = []
            dicoSeg[pattern].append(pattern)

    for name in dicoSeg.keys():
        tabGene = tab.loc[dicoSeg[name], :]

        outSum = pd.DataFrame(tabGene.sum(axis=0)).T

        outSum["Name"] = name

        frameout = pd.concat([frameout, outSum], ignore_index=False)
    frameout.set_index("Name", inplace=True)
    if log:
        frameout2 = frameout.copy()
        for cols in frameout2.columns:
            frameout2[cols] = frameout2[cols].astype('float64')
            frameout2[cols] = np.log2((frameout2.loc[frameout2[cols].notna(), cols]),
                                      dtype='float64')
        return frameout2
    else:
        return frameout


def concatOrderCov(order1ref1, orderRun1, design, segCol):
    """Fonction permettant de formater et concaténer le run de référence et le run en cours d'analyse pour créer le
    graphique en BOXPLOT
    Input:
    -Matrice ordonnée de référence (1 colnne Patient, 1 colonne Segment, 1 colonne de dont le nom est à spécifier
    (segcol)
    -Matrice ordonnée pour le run 1 colnne Patient, 1 colonne Segment, 1 colonne de valeur de ratio)
    -Design de CovCopCan afin de récupérer les information du chromosomes et start pour le trie dans le graphique
    Output:
    Matrice contenant toutes les données du jeu de référence et du run avel leur type associé (Reference/Donnees) et le
    chromosome associé à chaque gène
    """
    order1ref1['type'] = "Reference"
    orderRun1["type"] = "Donnees"

    order1ref = order1ref1.set_index(["Patient", segCol])
    orderRun = orderRun1.set_index(["Patient", segCol])

    newFrame = pd.concat([order1ref, orderRun], axis=0)
    newFrame.reset_index(inplace=True)
    for gene in pd.unique(design["gene"]):
        chromo = pd.unique(design.loc[design["gene"] == gene, "chromosome"].values)
        minstartGene = design.loc[design["gene"] == gene, "start"].min()
        newFrame.loc[newFrame[segCol] == gene, "chromosome"] = chromo
        newFrame.loc[newFrame[segCol] == gene, "start"] = minstartGene
    newFrame["chromosome"] = newFrame["chromosome"].astype('int')
    newFrame.sort_values(by=["chromosome", "start"], inplace=True)
    newFrame.drop(columns="start", inplace=True)
    return newFrame


def plotGraph(matrix, runName):
    """Génération du graphique en BOXPLOT grâce à la lib Plotly
    Inpyt:
    Matrice de sortie de la fonction concatOrderCov() contenant le jeu de données de référence et le jeu de données brut
    Output:
    Objet HTML contenant le graphique qui sera écrit dans le fichier HTML produit"""

    matrixref = matrix.loc[matrix['type'] == "Reference", :]
    matrixbrut = matrix.loc[matrix['type'] == "Donnees", :]
    # matrixbrut["Patient2"] = ""
    dico = {}
    listeBarre = []
    for chromo in pd.unique(matrix["chromosome"]):
        listeGenechro = matrix.loc[matrix["chromosome"] == chromo, "Segment"].to_list()
        lastGene = listeGenechro[-1]

        posGene = list(pd.unique((matrix["Segment"].to_list()))).index(lastGene)

        listeBarre.append(posGene + 0.5)

    for element in pd.unique(matrixbrut["Patient"]):
        if element[:-3] not in dico.keys():
            dico[element[:-3]] = []
        dico[element[:-3]].append(element)
        for element in dico.keys():
            indexVal = matrixbrut.loc[matrixbrut["Patient"].isin(dico[element])].index.to_list()
            matrixbrut.loc[indexVal, "Patient2"] = element
    fig = px.scatter(x=matrixbrut["Segment"], y=matrixbrut["Ratio"],
                     labels=dict(x="Segment", y="Depth ratio", color="Patient"))



    for element in listeBarre[:-1]:
        fig.add_vline(x=element)

    if len(pd.unique(matrixbrut["Patient2"])) == 1:
        fig.update_traces(visible=True, marker_symbol="circle", marker_size=10,marker_color='rgb(95, 44, 150)')
        #fig.update_layout(title_text=f"Run:{runName} | Patient :{pd.unique(matrixbrut['Patient2'])[0]}",
                          #title_font_size=20)
    else:
        fig.update_traces(visible="legendonly", marker_symbol="circle", marker_size=10)
        #fig.update_layout(title_text=f"Run:{runName}",
                         # title_font_size=20)

    fig.add_box(x=matrixref["Segment"], y=matrixref["Ratio"], name="Reference",marker_color='rgb(44, 150, 61)')  # ,boxpoints=False)
    fig.update_layout(showlegend=False)
    fig.update_xaxes(tickangle=90)

    plotObject = fig.to_html(full_html=False)
    return plotObject


#########################
# Z-SCORES#
#########################
def mad(a, med, axis):
    """Calcul du Median Absolute Deviation pour le z-score
    Input:
    -a: Les valeurs de ratio pour un segment données
    -med la médiane de profondeur pour ce segment
    -axis: l'axe sur la matrice à utiliser pour faire le calcul (sur les colonnes (1)/sur les lignes (0))
    Output:
    -MAD"""
    madi = np.median(np.absolute(a - med), axis=axis)

    return madi


def zscoreCalul(tabo):
    """"Calcul du z-score
    Principe: Itérer sur chaque segment et calculer la médiane, le MAD afin de calculer le z-score
    Input:
    Matrice contenant les ratio normalisé entre les patients et transformé en log
    Output:
    Matrice contenant les valeurs de z-scores pour chaque patients et gènes
    et contenant les valeurs de médiane et de MAD
    """
    segNameList = tabo.index.to_list()

    frameout = pd.DataFrame(columns=tabo.columns)

    for Nameseg in segNameList:
        tabGene = tabo.loc[Nameseg, :]

        outSeg = pd.DataFrame(tabGene).T

        median = outSeg.median(axis=1)[0]

        MAD = mad(outSeg, median, axis=1)[0]

        outSeg["Median"] = median
        outSeg["MAD"] = MAD

        frameout = pd.concat([frameout, outSeg], ignore_index=False)

    finalFrame = (0.6745 * (frameout.iloc[:, :-2].sub(frameout["Median"], axis=0)).div(frameout["MAD"], axis=0))

    finalFrame["MAD"] = frameout["MAD"]
    finalFrame["Median"] = frameout["Median"]
    return (finalFrame)


#########################
# COVCOPCAN#
#########################

def concatFrameCov(listeFilename):
    """Permet de récupérer une matrice rassemblement tous les résultats de CovCopCan dispersé dans les VCF
    Input:
    -Liste des fichiers à concaténer
    Output:
    -Dataframe contenant les données de tous les VCFs en rajoutant une colonne pour identifier le patient"""
    VCF_HEADER = ['CHROM', 'POS', 'ID', 'REF', 'ALT', 'QUAL', 'FILTER', 'INFO', "PATIENT"]
    empty_Frame = pd.DataFrame(columns=VCF_HEADER)
    for file in listeFilename:
        frame = VCF.dataframe(file)
        namepre = file.split("/")

        name = namepre[-1].split(".")[0]
        frame["PATIENT"] = name
        empty_Frame = pd.concat([empty_Frame, frame], ignore_index=True)
    return empty_Frame


def extract_end(string):
    """Subdivise la colonne info des VCFs de CovCopCan
    afin de récupérer l'information du end pour localiser le gène associé

    Input:
    -String de la valeur présente dans la colonne INFO du VCF
    Output:
    -String contenant la valeurs du END de segment ou un CNV est détecté par CovCopCan
    """

    split1 = string.split(";")[1]

    split2 = split1.split("=")[1]
    return split2


def identifyGene(framevcf, designframe, by="gene"):
    """A partir du fichier design, cette fonction peut associer les positions des CNVs indiquées dans les VCF de
     CovCopCan à leur nom
     Input:
     -Sortie de la fonction concatFrameCov()
     -Echelle à spécifier (defaut gène)
     Output:
     -Matrice contenant pour chaque ligne, le gène du panel associé à cet l'intervalle entre DEB et END"""

    framevcf["Segment"] = "NA"
    n = 0
    if by == "gene":
        for posDeb, posEnd in zip(framevcf["POS"], framevcf["INFO"]):
            end = int(extract_end(posEnd))

            verifDeb = designframe.loc[(designframe["start"] >= posDeb) & (designframe["end"] <= end), "gene"].values
            gene = ",".join(np.unique(verifDeb))

            framevcf.iloc[n, len(framevcf.columns) - 1] = gene
            n += 1
    if by == "amplicon":
        for posDeb, posEnd in zip(framevcf["POS"], framevcf["INFO"]):
            end = int(extract_end(posEnd))

            verifDeb = designframe.loc[
                (designframe["start"] >= posDeb) & (designframe["end"] <= end), "amplicon"].values
            gene = ",".join(np.unique(verifDeb))

            framevcf.iloc[n, len(framevcf.columns) - 1] = gene
            n += 1
    if by == "exon":
        for posDeb, posEnd in zip(framevcf["POS"], framevcf["INFO"]):
            end = int(extract_end(posEnd))

            verifDeb = designframe.loc[
                (designframe["start"] >= posDeb) & (designframe["end"] <= end), "amplicon"].values
            listeDeb = []
            for amplicon in verifDeb:
                nameEx = amplicon.split("Ex")[0]
                patternEx = amplicon.split("Ex")[1]

                while patternEx.isnumeric() == False:
                    patternEx = patternEx[:-1]

                exonName = nameEx + 'Ex' + patternEx
                if exonName not in listeDeb:
                    listeDeb.append(exonName)

            gene = ",".join(np.unique(listeDeb))

            framevcf.iloc[n, len(framevcf.columns) - 1] = gene
            n += 1
    return framevcf


def orderCovCop(tabidSeg, colUse):
    """A partir de la matrice contenant les résultats des VCFs, cette fonction va ordonner les valeurs en trois colonne
    Input:
    -Matrice des VCFs avec l'information des patients et des segments
    Output:
    -Matrice ordonnée avec une colonne par patient et segment avec la valeur des VCFs associées"""
    colNameFinal = f"{colUse}"
    dico = {"Patient": [], "Segment": [], colNameFinal: []}
    tabUse = tabidSeg.loc[:, ["ALT", "PATIENT", "Segment"]]

    for patient in pd.unique(tabUse["PATIENT"]):

        tabval = tabUse.loc[tabUse["PATIENT"] == patient, ["ALT", "Segment"]]

        for segment in tabval["Segment"]:
            cnv = tabval.loc[tabval["Segment"] == segment, "ALT"].values[0]

            listeSeg = segment.split(",")
            listePatient = [patient] * len(listeSeg)
            listecnv = [cnv] * len(listeSeg)

            dico["Patient"].extend(listePatient)
            dico["Segment"].extend(listeSeg)
            dico[colNameFinal].extend(listecnv)
    frame = pd.DataFrame(dico)

    frame.drop_duplicates(inplace=True)
    frame.set_index(["Segment", "Patient"], inplace=True)
    return frame


######## Seuils#################

def concatCovRef(tabrun, tabref):
    """Fonction permettant de concaténer une référence à la matrice de CovCopCan pour pouvoir ensuite utiliser cette
    matrice via l'executable .jar
    Input:
    - Matrice run 2D pour CovCopCan avec en index les amplicons et en colonne les patients
    - Matrice de référence en 2D avec en index les amplicons et en colonne les patients

    Output
    -Matrice utilisable par CovCopCan contenant les matrice de référence
    """
    listeUse = ["#Gene", "Target"]##########################################################################################Modifier #Gene
    if type(tabref) == pd.core.frame.DataFrame:
        tabrun2 = tabrun.set_index("Target")
        tabref2 = tabref.copy()

        allFrame = pd.concat([tabrun2, tabref2], axis=1)

        allFrame.reset_index(inplace=True)
        allFrame.rename(columns={"index": "Target"}, inplace=True)
        listePat = allFrame.columns[2:].to_list()
        listeUse.extend(listePat)
        frameFinal = allFrame.loc[:, listeUse]

        for col in frameFinal.columns[2:]:
            frameFinal[col] = frameFinal[col].astype("int64")
        return frameFinal
    else:
        return tabrun


def recupPathCov(path):
    """Cette fonction permet de récupérer le fichier nroamized de sortie de CovCopCan"""
    listeFile = os.listdir(path)
    for file in listeFile:
        if "normalized" in file:
            pathreturn = path + "/" + file
            return pathreturn
    txt = 'Votre dossier résultat de CovCopCan ne contient pas de fichier *normalized.tsv*'
    return txt


def concatCov(tab, genlist):
    """ A partir de la matrice concaténée des résultats de CovCopCan, permet de récupérer la moyenne des résultats de
    chaque gène et d'en faire un tableau pandas
    Input:
    -Matrice ordonnées contenant les résultats de la matrice normalized de CovCopCan
    -Liste de gène permettant de faire un subset de la matric, si vide, la matrice entière en renvoyé
    Output:
    -Matrice avec l'ensemble des moyenne de chaque gène pour chaque patient
    """
    newtab = pd.DataFrame(columns=tab.columns)

    for element in genlist:
        meanGene = tab.loc[tab.index.str.contains(element)].iloc[:, 0:].mean(axis=0)
        bis = pd.DataFrame(meanGene).T
        bis["Name"] = element
        newtab = pd.concat([newtab, bis])
    newtab.set_index("Name", inplace=True)
    return newtab


#########################
# FONCTIONS COMPLÉMENTAIRES#
#########################

def format_Matrix(tabpath, colindex, colcolpre, colval, libcol):
    """Permet de récupérer une matrice en 2D à partir des fichiers amplicons_coverage disponibles en fin de pipeline
    Input:
    - Path du fichier Matrice
    - Colindex: Colonne des amplicons
    - colcolpre: Colonne des samples
    - colval : Colonne des valeur
    - libcol: Colonne de la library
    Output:
    -Matrice en 2D avec les amplicons en index et les samples en colonne"""

    tab = pd.read_csv(tabpath, sep=";", low_memory=True)
    tab.drop_duplicates(inplace=True, ignore_index=True)  # remove possible duplicates global line
    colcol = colcolpre + "_2"

    tab[colcol] = tab[colcolpre] + "-" + tab[libcol]
    tab.loc[:, colval] = tab[colval].astype('float')
    tab.sort_values(colcol, inplace=True)
    indextab = pd.unique(tab[colindex])
    col = pd.unique(tab[colcol])
    dico = {}
    for colonne in col:
        if "TBEAU" in colonne or "TPCBRCA" in colonne or "SC" in colonne:
            continue
        tabcol = tab.loc[tab[colcol] == colonne, :]
        if colonne not in dico.keys():
            dico[colonne] = []
        for ampli in indextab:
            valuePatient = tabcol.loc[tabcol[colindex] == ampli, colval].sum(axis=0)

            dico[colonne].append(valuePatient)
    # dicoIndex={"Name":indextab}

    frame = pd.DataFrame(dico, index=indextab)

    frame.rename_axis('Names', inplace=True)
    # frame.replace(0,np.nan,inplace=True)
    return frame


def QCpatient(tabpre, treshQCfunc):
    """Permet d'éliminer les patients de mauvaise qualité
    Input:
    -Matrice 2D amplicon vs patient
    -Seuil à utiliser pour éliminer les patients"""
    tab = tabpre.copy()
    sommePat = pd.DataFrame(tab.sum(axis=0))
    listePat = sommePat.loc[sommePat[0] < treshQCfunc, :].index.to_list()
    if listePat != []:
        print(f"Patient ne passant pas le filtre de qualité: {listePat}")
    tabpre.drop(columns=listePat, inplace=True)
    return tabpre, listePat


def changeOrder(tab, cat, subcat, value):
    """Permet de passer d'une matrice en deux dimension à une matrice avec trois colonnes
    Attention : Il  faut s'assurer que la colonne subcat soit bien l'index de la matrice"""

    dico = {cat: [], subcat: [], value: []}
    interList = []

    for idi in tab.columns:
        segment = tab.index
        val = tab.loc[:, idi].values
        interList.append(idi)

        interList2 = interList * len(segment)
        interList = []

        dico[cat].extend(interList2)
        dico[subcat].extend(segment)

        dico[value].extend(val)

    frame = pd.DataFrame(dico)

    return frame


def selectSegment(tabSelect):
    """Sélectionner les gènes si dans la colonne 'segment' il y a un 'y'
    -Output:
    Liste de gène à utiliser"""
    segment = pd.unique(tabSelect.loc[tabSelect["segment"] == "y", "segment"].to_list())

    return segment


def addCyto(frameFinal, cytofile, designcov, geneUse):
    """Permet de rajouter les données cytogéiques aux données
    Input:
    -Matrice des résultats
    -Cytofile fichier contenent les bandes cytogéniques sans header
    -DesignCovCopCan
    -Liste de Gene à vérifier

    -Output
    Matrice finale avec les bandes cytogénique associées aux gènes"""
    designcov.sort_values('start', inplace=True, ascending=False)
    dicocyto = {}
    frameFinal2 = frameFinal.copy()

    for chro in pd.unique(designcov.chromosome):
        sousTab = designcov.loc[designcov["chromosome"] == chro, :]
        chrostr = "chr" + str(chro)
        bandcyto2 = cytofile.loc[cytofile[0] == chrostr, :]

        for gene in pd.unique(sousTab.gene):
            start = sousTab.loc[sousTab["gene"] == gene, "start"].to_list()[0]
            end = sousTab.loc[sousTab["gene"] == gene, "end"].to_list()[-1]
            # Un gène sur deux bande cyto ?
            bandValuespre = bandcyto2.loc[((bandcyto2[1] >= start) & (bandcyto2[2] <= end)) |
                                          ((bandcyto2[1] <= start) & (bandcyto2[2] >= end)), 3].values
            if len(bandValuespre) == 0:
                bandValues = "NoBand"
            else:
                bandValues = bandValuespre[0]
            dicocyto[gene] = bandValues
    frameFinal2["Cytoband"] = ""
    for segment in dicocyto.keys():
        frameFinal2.loc[frameFinal2["Segment"] == segment, "Cytoband"] = dicocyto[segment]
    framereturn = frameFinal2.loc[frameFinal2["Segment"].isin(geneUse), :]

    return framereturn


def ajustement(tab, designcov, nameChr, nameCyto, nameSeg,namePat, nameLib, colonneValueOrder):
    """Permet de récupérer une matrice final (tab) où les vaeurs sont triées selon le chromosome et start
    Input:
    -Matrice final
    -Design de CovCopCan pour récupérer les information de chr et start
    -L'ordre des colonne que l'on souhaite avoir sur le HTML final
    ATTENTION: Les noms doivent correspondrent à ceux qui sont dans tab, si vous souhaitez changez de nom aux headers,
    vous pouvez le faire sur les fonction qui ordonne les tables (ex:changeOrder), puis aussi dans cette liste

    Output:
    Matrice où les valeurs sont triées selon le chromosome et le start"""

    designcov.sort_values(["chromosome", "start"], inplace=True)
    dico = {}

    colonneOrder = [nameChr, nameCyto, nameSeg,namePat, nameLib]
    colonneOrder.extend(colonneValueOrder)
    for gene in pd.unique(designcov.gene):
        chromoGene = designcov.loc[designcov["gene"] == gene, "chromosome"].to_list()[0]
        startGene = designcov.loc[designcov["gene"] == gene, "start"].to_list()[0]
        if gene not in dico.keys():
            dico[gene] = []
        dico[gene].extend([chromoGene, startGene])
    for segment in pd.unique(tab.Segment):
        tab.loc[tab["Segment"] == segment, nameChr] = dico[segment][0]
        tab.loc[tab["Segment"] == segment, "start"] = dico[segment][1]

    dicoPatLib={}
    for patient in pd.unique(tab.Patient):
        print(patient)

        numlib=patient.split("-")[-1]
        dicoPatLib[patient]=numlib
    for patient in dicoPatLib.keys():
        tab.loc[tab["Patient"] == patient, nameLib] = dicoPatLib[patient]

    tab.rename(columns={"Cytoband": nameCyto, "Segment": nameSeg, "Patient": namePat}, inplace=True)
    colonneOrder.append("start")
    print(tab)

    print(colonneOrder)
    tabreturn = tab.loc[:, colonneOrder]
    tabreturn[nameChr] = tabreturn[nameChr].astype('int64')

    tabreturn.sort_values([nameChr, "start"], inplace=True, ignore_index=True)
    tabreturn.drop(columns=["start"], inplace=True)

    print(tabreturn)
    return tabreturn


def results_to_HTML(plotlytObject, frameFinal, projectName, dicoPathIMG, outputPath, seuilZscore,
                    seuilBasCov, seuilHautCov,minusPat,plusPat,patName,strNotanalyzed):
    
    frameFinal.replace(np.nan, "-", inplace=True)
    ###Modification sur l'objet de Plotly
    plotlytObject = plotlytObject.replace('''<div>''','''<div class="plotGraph">''')
    #plotlytObject = plotlytObject.replace("</div>", "")
    plotlytObject = plotlytObject.replace("</body>", "")
    plotlytObject = plotlytObject.replace("</html>", "")

    head = '''
    <!DOCTYPE html>
    <html>
    <head>
        <title> CNV results</title> 
        <meta charset="utf-8"/>
        <style>

            #headerStyle {
                display:flex;
                justify-content:space-around;
                background-color:#754fe8;
                margin-top:0px;
                font-size:1.2em;
                height:40px;
                align-items:center;
                position: relative;
                font-family:Arial;
            }
            p.notAnalyzed{
                font-family:Arial;
                width:100%;

            }
            
            #topPage {
                height:50vh;
                width:100%;
                background-color:white;
                margin-top:1px;
            }
            .plotGraph{
                height:100%;
                width:50%;
                float:left;
                margin-top:1px;
                margin-right:0px;
                background-color:white;
            }
            #myInput {
            width: 30%; /* Full-width */
            font-size: 13px; /* Increase font-size */
            padding: 15px; /* Add some padding */
            border: 3px solid #ddd; /* Add a grey border */
            margin-top: 5px;
            margin-bottom: 3px; /* Add some space below the input */
            margin-left: 5px;
            position:sticky;
            position: -webkit-sticky;
            top:0;
            }

            .table-wrapper{
            overflow-y:auto;
            width:50%;
            height:100%;
            text-align: center;
            font-size:15px;
            font-family:Arial;
            background-color:white;
            }      
            .table-wrapper th{
                position: sticky;
                top: 0;
                word-wrap:break-word;
                color: white;
                background-color: #2c963d;
                padding:0px;
            }
            #styleTable{
                width:100%;height:50%;overflow:scroll;margin-left:1px;background-color:#754fe8;}
            #styleTable td{
                padding: 0; 
                margin: 0;
            }
            #styleTable tr:nth-child(even) {
            background-color: #eee;
            }
            #styleTable tr:nth-child(odd) {
            background-color: #fff;
            }
            td.ampli{
            background-color: Tomato;
            width:100px;
            word-wrap: break-word;
            }
            td.del{
            background-color: DodgerBlue;
            width:100px;
            word-wrap: break-word;
            }

            h1{
                font-size:30px;
                color:white;
            }
            a {
            color:white; 
            }
            .littleColInfo{
                width:15px;
                word-wrap: break-word;
            }
            .intermediateColInfo{
                width:20px;
                word-wrap: break-word;
            }
            .bigColInfo{
                width:80px;
                word-wrap: break-word;
            }
        </style>
    </head>
   '''
    header=f'''
    <header id="headerStyle">
    <div><a href="./{minusPat}_resultat.html">{minusPat}</a></div>
    <div class="styleHeaderDiv"> <h1>{projectName} | {patName}</h1> </div>
    <div><a href="./{plusPat}_resultat.html">{plusPat}</a></div>
    </header>
    <body>'''
    textPatientSOLO = f'''
            <div id="topPage">
            {plotlytObject}
            <div class='table-wrapper'>
            <input id="myInput" type='text' onkeyup="researchGene()" placeholder="Search a segment...">
            <table id="styleTable"> 
            <thead>
            <tr>
    '''
    file = open(f"{outputPath}{patName}_resultat.html", "w")
    file.write(head)
    file.write(header)
    file.write(textPatientSOLO)
    for index, col in enumerate(frameFinal.columns):
        if col=="Chr" or col=="Lib":
            colpart = f'''\t\t\t<th class="littleColInfo">{col}</th>\n'''
        elif col=="BandCyto" or col=="Segment":
            colpart = f'''\t\t\t<th class="intermediateColInfo">{col}</th>\n'''
        else:
            colpart = f'''\t\t\t<th class="bigColInfo">{col}</th>\n'''
        file.write(colpart)
        if col == frameFinal.columns[-1]:
            file.write('''\t\t</tr>
            </thead>''')

    for line in frameFinal.index:
        file.write('''\t\n\t\t<tr>\n''')
        dicVal = frameFinal.iloc[line, :].to_dict()
        n = 0

        for key in dicVal.keys():
            n += 1
            if type(dicVal[key]) == float or type(dicVal[key]) == np.float64:
                shortVal = round(dicVal[key], 4)
                dicVal[key] = shortVal
            if type(dicVal[key]) != str:
                if "ZScore_" in key:
                    if dicVal[key] > seuilZscore:
                        file.writelines(f'''\t\t<td class="ampli">{dicVal[key]}</td>\n''')
                        continue
                    if dicVal[key] < -seuilZscore:
                        file.writelines(f'''\t\t<td class="del">{dicVal[key]}</td>\n''')
                        continue
                if  "TreshCov_" in key:
                    if dicVal[key] < seuilBasCov:
                        file.writelines(f'''\t\t<td class="del">{dicVal[key]}</td>\n''')
                        continue
                    if dicVal[key] > seuilHautCov:
                        file.writelines(f'''\t\t<td class="ampli">{dicVal[key]}</td>\n''')
                        continue
                      
            elif "VarCol_" in key:
                if "DUP" in dicVal[key]:
                    file.writelines(f'''\t\t<td class="ampli">"DUP"</td>\n''')
                    if n != len(dicVal.keys()):
                        continue
                    else:
                        file.writelines('''\t\t</tr>''')
                        break
                      
                if "DEL" in dicVal[key]:
                    file.writelines(f'''\t\t<td class="del">"DEL"</td>\n''')
                    if n != len(dicVal.keys()):
                        continue
                    else:
                        file.writelines('''\t\t</tr>''')
                        break
                      
            file.write(f'''\t\t\t<td>{dicVal[key]}</td>\n''')

    file.write(f'''
    </table>
    </div>
    </div>
    <p class="notAnalyzed"><strong>Patient not analyzed</strong>: {strNotanalyzed}</p>
    ''')

    for param in dicoPathIMG.keys():
        print(dicoPathIMG)
        if dicoPathIMG[param] != []:
            for filePath in dicoPathIMG[param]:
                fileName = filePath.split("/")[-1]
                fileName2 = fileName.split('.')[0]
                image = open(filePath, "rb").read()
                image_base64 = base64.b64encode(image)
                image_base64 = image_base64.decode()
                file.write(f'''<div>
                <h2 style="text-align:center";>{fileName2} | {param}</h2>
                <img style="display:block;margin:auto" src="data:image/png;base64,{image_base64}"/></div>
                </body>''')
    file.write('''
    <script>
    function researchGene() {
      // Declare variables
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("styleTable");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
    
    </html>''')
    file.close()

def Main(pathJava, pathrecouMatrix, pathrunRefRatio, listpathRunRefZscore,
         listpathRunRefCovCopCan, colListCov, colListZ, pathMatrixCovCopCan, pathdesignCovCopCan, selectGenePath,
         outputPath, runName, execCov, cytoPath, treshList, treshQC):
    print("Running...")
    listRefCov = []
    patentrefCov = []
    listRefZscore = []
    patentrefZsc = []
    print(outputPath)
    try:
        os.makedirs(outputPath + runName)
    except FileExistsError:
        runName = input("Le run est déjà existant. Renommez votre projet : ")
        os.makedirs(outputPath + runName)

    ###IMPORT DES FICHIERS COMPLEMENTAIRES
    cytofile = pd.read_csv(cytoPath, sep='\t', header=None)
    runRef = pd.read_csv(pathrunRefRatio, sep="\t", index_col=0)
    design = pd.read_table(pathdesignCovCopCan, sep="\t")

    if listpathRunRefCovCopCan != []:

        for pathRefCov in listpathRunRefCovCopCan:
            refCov = pd.read_table(pathRefCov, sep='\t', index_col=0)
            listePatRefCov = refCov.columns.to_list()

            patentrefCov.append(listePatRefCov)
            listRefCov.append(refCov)
    else:
        listRefCov = []
        patentrefCov = []

    if listpathRunRefZscore != []:
        for zPathRef in listpathRunRefZscore:
            refZscore = pd.read_table(zPathRef, sep='\t', index_col=0)
            listePatZRef = refZscore.columns.to_list()

            listRefZscore.append(refZscore)
            patentrefZsc.append(listePatZRef)

    ####BLOCK MATRICE RECOUVREMENT#####

    runMatrixPre = format_Matrix(pathrecouMatrix, "amplicon", "sample", "dp_median", "library")

    outputPathName = outputPath + "/" + runName + "/"

    runMatrixPre.to_csv(f"{outputPathName}{runName}Matrix.tsv", sep="\t")

    resulQC = QCpatient(runMatrixPre, treshQC)
    listeOutPat = resulQC[1]
    runMatrix = resulQC[0]

    strOutpat=", ".join(listeOutPat)
    #file = open(f"{outputPathName}{runName}_QC_REJECT.txt", 'w')
    #file.write(str(listeOutPat))
    #file.close()

    runMatrix.to_csv(f"{outputPathName}{runName}_CorrMatrix.tsv", sep="\t")

    ###Création d'une liste de patient
    patentList = []
    patentdico={}

    for sample in runMatrix.columns.to_list():
        pat = sample[:-3]
        if pat not in patentList:
            patentList.append(pat)
        if pat not in patentdico.keys():
            patentdico[pat]=[]
        patentdico[pat].append(sample)

    ###BLOCK COVCOPCAN#####
    #### Retire les samples qui ont pas passé le QC
    matCovCop = pd.read_csv(pathMatrixCovCopCan, sep="\t")
    for pat in listeOutPat:
        if pat in matCovCop.columns.to_list():
            matCovCop.drop(columns=[pat], inplace=True)

    matCovCop.drop_duplicates(inplace=True, ignore_index=True)  # remove possible duplicates

    pathCovInput = outputPath + "/" + runName + "/"

    #######ICI POUR CONCATÉNER LA MATRICE DE REFERENCE####
    listResulCov = []
    for tabRef, nameCol in zip(listRefCov, colListCov):
        pathCovresult = outputPath + "/" + runName + "/" + runName + f"{nameCol}_CovCopCan_Result"
        listResulCov.append(pathCovresult)
        pathCovMatCorr = pathCovInput + "/" + runName + f"{nameCol}_Cov_MatrixCorr.tsv"
        matCovCop.to_csv(pathCovMatCorr, sep="\t", index=None)

        matrixForCovCopCan = concatCovRef(tabrun=matCovCop, tabref=tabRef)

        pathCovMatCorrUse = pathCovInput + "/" + runName + f"{nameCol}_Cov_MatrixCorrUse.tsv"
        matrixForCovCopCan.to_csv(pathCovMatCorrUse, sep='\t', index=None)
        colRefStr = " ".join(tabRef.columns.to_list())
        os.system(f"{pathJava}/java -jar {execCov} -g -d {pathdesignCovCopCan} -m {pathCovMatCorrUse}"
                  f" -o {pathCovresult} "
                  f" -c {colRefStr} "
                  f" --showCusum true --cleanMinValue 0 --showAmpliconsDots false --showMeanGene true"
                  f" --deletionThreshold {treshList[1]} --duplicationThreshold {treshList[2]} --minCNVLength 5")

    print("Fin de l'analyse de CovCopCan. Traitement des résultats ....")
    listResultCovNorm = []

    for path, patentRef in zip(listResulCov, patentrefCov):
        colAsuppr = ["Gene", "Filter"]
        colAsuppr.extend(patentRef)
        #path=path+"/"
        pathCovCopCanNorm = recupPathCov(path)
        matrixCovCopPreTab = pd.read_table(pathCovCopCanNorm, sep="\t")
        matrixCovCopPreTab.drop(columns=colAsuppr, inplace=True)
        selectCovCop = matrixCovCopPreTab.iloc[:, 4:-1]
        listResultCovNorm.append(selectCovCop)
        colAsuppr = []
    #### FIN DE LA RECUPERATION DES MATRICES RESULTATS DE COVCOPCAN dans listResultCovNorm #####


    print('Traitement des ratios en cours ...')
    ####BLOCK SELECTION DE GENE & RATIO DE PROFONDEUR#####

    ratioRun = ratioGeneSumMedTrans(runMatrix, by="gene", log=False)
    ratioRef = ratioGeneSumMedTrans(runRef, by="gene", log=False)
    geneUse = ratioRun.index.to_list()
    if selectGenePath != "":
        selectGene = pd.read_csv(selectGenePath, sep='\t')
        geneUse = selectGene.loc[selectGene["cnv"] == "y", "gene"].to_list()



    orderRun = changeOrder(ratioRun.loc[geneUse, :], "Patient", "Segment", "Ratio")
    orderRef = changeOrder(ratioRef.loc[geneUse, :], "Patient", "Segment", "Ratio")

    ####BLOCK DES FIGURES####
    concatFrameTab = concatOrderCov(orderRef, orderRun, design, "Segment")

    # concatFrameTab.to_csv(f"{outputPathName}{runName}_FrameForBoxPlot.tsv", sep="\t", index=False)

    orderRun.set_index(["Segment", "Patient"], inplace=True)
    print("Calcul du z-score en cours ...")

    listResulZscore = []
    colZ = []
    if not listpathRunRefZscore:

        runZscore = runMatrix
        ratioRunlog = ratioGeneSumBase(runZscore, by="gene", log=True)
        resultZscoreRun = zscoreCalul(ratioRunlog)
        resultZscoreRun2 = resultZscoreRun.iloc[:, :-2]
        nameUseOrder = "Zscore"
        colZ.append(nameUseOrder)
        orderZscore = changeOrder(resultZscoreRun2.loc[geneUse, :], "Patient", "Segment", nameUseOrder)
        orderZscore.set_index(["Segment", "Patient"], inplace=True)
        listResulZscore.append(orderZscore)
    #####Z-scores####
    else:
        for tabRef, patentrefZsc, nameColZ in zip(listRefZscore, patentrefZsc, colListZ):
            runZscore = pd.concat([runMatrix, tabRef], axis=1)
            ratioRunlog = ratioGeneSumBase(runZscore, by="gene", log=True)
            resultZscoreRun = zscoreCalul(ratioRunlog)
            resultZscoreRun2 = resultZscoreRun.iloc[:, :-2]
            resultZscoreRun2.drop(columns=patentrefZsc, inplace=True)

            nameUseOrder = f"ZScore_{nameColZ}"

            colZ.append(nameUseOrder)
            orderZscore = changeOrder(resultZscoreRun2.loc[geneUse, :], "Patient", "Segment", nameUseOrder)
            orderZscore.set_index(["Segment", "Patient"], inplace=True)
            listResulZscore.append(orderZscore)

    ##TRAITEMENT DES RESULTATS DE COVCOPCAN####
    ######CovCopCan (Seuil)####Fonction
    # A ce stade j'ai:
    # Un frame en colonne avec les ratio de profondeur
    # Une liste de Frame avec CovCopCan non ordonnée
    # Liste de Frame avec le z-score avec des frame ordonnées

    ######TRAITEMENT DES RESULTATS DE COVCOPCAN######
    listOrderCov = []
    treshCol = []
    for resultCov, nameResultCov in zip(listResultCovNorm, colListCov):
        resultCov.set_index("Amplicon", inplace=True)
        meanCov = concatCov(resultCov, geneUse)
        nameUseResultCov='TreshCov_'+nameResultCov
        orderMeanCov = changeOrder(meanCov, 'Patient', 'Segment', nameUseResultCov)
        orderMeanCov.set_index(["Segment", "Patient"], inplace=True)
        colName = orderMeanCov.columns.to_list()[-1]

        treshCol.append(colName)
        listOrderCov.append(orderMeanCov)

    del listResultCovNorm  ## Vider la mémoire de la variable

    #####CovCopCan (VarPatient)#######
    listOrderCovVar = []
    varCol = []

    for pathResulCovCop, nameCol in zip(listResulCov, colListCov):
        pathVCF = pathResulCovCop + "/VCF/*vcf"

        listVCF = glob.glob(pathVCF)
        concatVCF = concatFrameCov(listVCF)
        idSeg = identifyGene(concatVCF, design, by="gene")
        nameColUse="VarCol_"+nameCol
        orderICovCopCan = orderCovCop(idSeg, nameColUse)
        colName = orderICovCopCan.columns.to_list()[-1]

        varCol.append(colName)
        listOrderCovVar.append(orderICovCopCan)
    #######CONCATÉNATION DES RESULTATS#####
    ListAllFrame = []
    ListAllFrame.extend(listResulZscore)
    ListAllFrame.extend(listOrderCov)
    ListAllFrame.extend(listOrderCovVar)
    frameFinalbis = pd.concat(ListAllFrame, axis=1)

    frameFinalbis.reset_index(inplace=True)

    frameCyto = addCyto(frameFinalbis, cytofile, design, geneUse)

    AllColCov = []
    AllColCov.extend(treshCol)
    AllColCov.extend(varCol)

    listeSortAllCov = sorted(AllColCov)
    colZ.extend(listeSortAllCov)

    frameReturn = ajustement(frameCyto, design, nameChr="Chr", nameCyto="BandCyto",
                             nameSeg="Segment", nameLib="Lib",namePat="Patient", colonneValueOrder=colZ)

    ###PLOT GENERAL####
    #plotlytObjectMainGene = plotGraph(concatFrameTab, runName=runName)
    #frameReturn.to_csv(f"{outputPathName}{runName}_resultat_tool.tsv", sep="\t", index=None)

   # results_to_HTML(plotlytObject=plotlytObjectMainGene, frameFinal=frameReturn, runName=runName,
                    #listPathIMG=[], outputPath=outputPathName, seuilZscore=treshList[0],
                    #seuilBasCov=treshList[1], seuilHautCov=treshList[2])

    htmlOutput = outputPathName + "HTML_Files/"
    os.makedirs(htmlOutput)

    pathCovresult = outputPath + "/" + runName + "/" + runName + f"{nameCol}_CovCopCan_Result"
    listResulCov.append(pathCovresult)


    dicoParamCov={}

    for pathResultCov,nameParam in zip(listResulCov,colListCov):
        pathIMG =f"{pathResultCov}/Charts/*png"
        listCHARTpre = glob.glob(pathIMG)
        listCHART = sorted(listCHARTpre)
        dicoParamCov[nameParam]=listCHART
        print(dicoParamCov)

    dicoPatChart = {}

    for patent in patentList:
        dicoParam2 = {}
        for param in dicoParamCov.keys():
            for samplePNG in dicoParamCov[param]:
                patent2=patent+"-"
                if patent2 in samplePNG:
                    if param not in dicoParam2.keys():
                        dicoParam2[param]=[]
                    dicoParam2[param].append(samplePNG)
        dicoPatChart[patent]=dicoParam2

    print(patentdico)
    listKey=sorted(list(patentdico.keys()))
    print(listKey)
    for index,patent in enumerate(listKey):
        print(patentdico[patent])
        
        print("##############")

        print(concatFrameTab.loc[concatFrameTab["Patient"].isin(patentdico[patent]),:])
        frameSample = concatFrameTab.loc[((concatFrameTab["Patient"].isin(patentdico[patent])) |
                                          (concatFrameTab["type"] == "Reference")), :]
        frameReturnsample = frameReturn.loc[frameReturn["Patient"].isin(patentdico[patent]), :]

        frameReturnsample.reset_index(drop=True, inplace=True)
        plotlytObjectMainGene = plotGraph(frameSample, runName=runName)
        frameReturnsample2=frameReturnsample.drop(columns=["Patient"])
        print(index)
        if index==0:
            minusPat=""
            plus=index+1
            plusPat=listKey[plus]
        elif index==len(listKey)-1:
            plusPat=""
            minus=index-1
            minusPat=listKey[minus]
        else:
            minus=index-1
            minusPat=listKey[minus]
            plus=index+1
            plusPat=listKey[plus]

        
        results_to_HTML(plotlytObject=plotlytObjectMainGene, frameFinal=frameReturnsample2, patName=patent,
                        dicoPathIMG=dicoPatChart[patent], outputPath=htmlOutput, seuilZscore=treshList[0],
                        seuilBasCov=treshList[1], seuilHautCov=treshList[2],minusPat=minusPat,plusPat=plusPat,
                        projectName=runName,strNotanalyzed=strOutpat)
    return


parser = argparse.ArgumentParser()
parser.add_argument("-m", "--matrice", help="Chemin de la matrice de recouvrement amplicon_coverage", type=str,
                    required=True)
parser.add_argument("-c", "--matricecov", help="Chemin de la matrice de recouvrement sous le format de CovCopCan",
                    required=True, type=str)
parser.add_argument("-d", "--design", help="Chemin de la matrice du design specifique a CovCopCan", type=str,
                    required=True)
parser.add_argument("-r", "--reference", help="Chemin de la référence pour la visualition en boxplot", type=str,
                    required=True)

parser.add_argument("-rZ", "--refZsc", help="Chemin(s) de la matrice de reference 1 pour le calcul du z-score",
                    nargs="+",type=str,required=False)

parser.add_argument("-nZ", "--nameZ",
                    nargs="+",type=str,
                    help="Nom des colonnes à utiliser pour le tableau (dans l'ordre des tables de références du Zscore)"
                    ,required=False)
parser.add_argument("-rC", "--refCov", help="Chemin(s) de la référence pour l'utilisation de CovCopCan", nargs="+",
                    type=str,required=False)
parser.add_argument("-nC", "--nameC",
                    help="Nom des colonnes à utiliser pour le tableau "
                         "(dans l'ordre des tables de références du CovCopCan)",nargs="+",type=str,required=False)
parser.add_argument("-e", "--executable", help="Chemin du fichier jar de CovCopCan", type=str, required=True)
parser.add_argument("-b", "--cytoband", help="Chemin du fichier contenant les cytoband", type=str, required=True)
parser.add_argument("-o", "--output", help="Dossier contenant les sortie des outils", type=str, required=True)

parser.add_argument("-g", "--validgene", help="Fichier contenant les gènes à visualiser", type=str, required=False,
                    default="")
parser.add_argument("-u", "--run", help="Nom du run en cour d'utilisation", type=str, required=True)
parser.add_argument("-q", '--QC', help="Seuil correspondant à la somme minimale nécessaire pour être traité",
                    type=int, required=False, default=20000)
parser.add_argument("-t", '--treshold',
                    help="Liste des seuils à utiliser pour la détection des CNVs "
                         "[Z-score, DeletionCovCopCan, AmplificationCovCopCan]",
                    nargs="+",type=int, required=False, default=[3.5, 0.5, 1.5])
parser.add_argument("-jp", "--pathjava", help="Chemin binaire java", required=True, type=str)

args = parser.parse_args()

if __name__ == '__main__':
    print(args.refZsc)
    print(args.refCov)
    print(args.nameC)
    print(args.nameZ)
    print(args.run)
    Main(pathrecouMatrix=args.matrice, pathrunRefRatio=args.reference, listpathRunRefZscore=args.refZsc,
         listpathRunRefCovCopCan=args.refCov, colListCov=args.nameC, colListZ=args.nameZ,
         pathMatrixCovCopCan=args.matricecov, pathdesignCovCopCan=args.design, selectGenePath=args.validgene,
         outputPath=args.output, runName=args.run, execCov=args.executable, cytoPath=args.cytoband,
         treshList=args.treshold, treshQC=args.QC, pathJava=args.pathjava)
#python ./tool_CovRaZ.py -m ./Inputtool/sousSetData.csv -e ./Inputtool/CovCopCan-1.3.3.jar
# -r /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetPass1.tsv
# -rZ /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetAllRun.tsv
# /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetPass1.tsv
# /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetFalseDatast.tsv
# -nZ ALL Pass1 RAND -rC /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetPass1.tsv
# /home/pierre/Desktop/localpgueracher_cnv/Input_files/subDatasets/sousSetFalseDatast.tsv
# -nC PASS1 RAND -c ./Inputtool/CovCopSousSet.tsv -d ./Inputtool/designSousSet.tsv
# -b ./Inputtool/cytoBand.txt  -o ./TestOutput/ -u TestModifReplicat -jp /usr/bin/
